---
title: Final analysis report
author: Marco Dalla Vecchia
date: 05/10/2022
geometry: "left=3cm,right=3cm,top=2cm,bottom=2cm"
# generate pdf with: pandoc final_report.md -o report.pdf
---

# Image segmentation
## Description
Image blobs.tif was segmented using an automatic threshold method as seen in 01_Segmentation_AnalyzeParticles.ijm.

## Output
![Overlay of original image with detected particles (in pink) labeled with corresponding label numbers.](./../output/segmentation/segmentation_overlay.png)

# Particle measurements
## Description
Relevant properties were measured for each detected particle in 01_Segmentation_AnalyzeParticles.ijm. Some criteria on the size was used to eliminate very small particles.

## Output
Table can be found in _output/measurements/measurements.csv_

# Measurements summary statistics
## Description
A brief summary statistics of the calculate particles properties was computed via 02_DescriptiveStatistics.py.

## Output
Table can be found in _output/measurements/descriptive_statistics.csv_

# Measurements figure making
## Description
Some relationships of interest between the measured properties were plotted in a single figure using _scripts/03_PlotMeasurements.py_.

## Output
![Data exploration. Top left - linear relationship between particles area and perimeter. Top right - distribution of mean intensity values across three area categories. Bottom left - distribution of mean intensity of al particles. Bottom right - as previous panel by divided in three area categories.](./../output/figures/exploratory_plots.pdf)