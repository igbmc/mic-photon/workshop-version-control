
# Workshop version control repository
This is a test repository with a simple workflow to practice version control and to establish good practices.

This workshop goes in conjuction with the material present at [version-control-gitlab repository](https://gitlab.com/igbmc/mic-photon/version-control-gitlab).

Main theoretical intro [slides here](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/)

Step-by-step guide on Gitlab [slides here](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/gitlab-step-by-step)

Workshop backbone [slides here](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/handson-workshop)

## Author
Marco Dalla Vecchia (dallavem.igbmc.fr)

## Installation
### ImageJ
Fiji (ImageJ) can be downloaded from [here](https://imagej.net/software/fiji/downloads). Imagej macros (.ijm) files can be used via drag-n-drop into the main Fiji window and by pressing _Run_ in the macro editor.

### Python
To use the Python code the use of **miniconda virtual environment** is recommended.

Download and install miniconda following [the instructions](https://docs.conda.io/en/latest/miniconda.html), then simply create a python virtual environment with all the required packages using from the terminal/anaconda shell:

```
$ conda env create -f env.yml 
```

This will create an environment called workshop_env. To activate it:

```
$ conda activate workshop_env
```

And finally to run the scripts:

```
# inside the scripts folder
# with workshop_env activated

$ python 02_DescriptiveStatistics.py
$ python 03_PlotMeasurements.py
```

## Report
I added a _mock_ report to illustrate that a final production-quality report of the executed analysis can be readily produced using the content of the repository. To produce the final pdf file a conversion step from markdown was done using **Pandoc**. You can install it following instructions [here](https://pandoc.org/installing.html).

## Structure

```bash
├── data
├── output
│   ├── figures
│   ├── measurements
│   └── segmentation
├── report
└── scripts
```

**data**: contains image to be analyzed.

**scripts**: contains all the python and imagej scripts needed for the analysis.

**output**: contains all the output of the analysis.

**report**: contains the source (.md) and final (.pdf) report for the executed analysis.

## Workflow description

- 01_Segmentation_AnalyzeParticles.ijm: load image, segment it and calculate particles properties. Export segmentation overlay and measurements table.

- 02_DescriptiveStatistics.py: simple script to generate descriptive statistics of previously exported measurements table.

- 03_PlotMeasurements.py: create a nice visualization of some interesting relationships in the measurements table.

- Finally, generate the final report by modifying the final_report.md file and convert it to pdf using: 

```
$ pandoc final_report.md -o report.pdf
```

### Tasks
hands-on.md contains suggested activity/modifications to the repository during the workshop.